import os
import cv2
import numpy as np

def open_images (base_path, img_name):
  csv = open('../data_images.csv', 'a+')

  xb = np.array(cv2.imread(f"{base_path}/soja_blue.png")[:][:,:,0])
  xb = np.reshape(xb, (1,400))

  xg = np.array(cv2.imread(f"{base_path}/soja_green.png")[:][:,:,0])
  xg = np.reshape(xg, (1,400))

  xr = np.array(cv2.imread(f"{base_path}/soja_red.png")[:][:,:,0])
  xr = np.reshape(xr, (1,400))

  xn = np.array(cv2.imread(f"{base_path}/soja_nir.png")[:][:,:,0])
  xn = np.reshape(xn, (1,400))

  row = [xb,xg,xr,xn]
  row = np.reshape(row, (1, 1600))

  line = str([img_name, list(row[0]), 'soja'])
  line = line.replace('[', '').replace(']','')+';\n'
  csv.write(line)
  csv.close()

def write_row_csv(path_to_dir):
  name_dir = path_to_dir.split('/')[-1:][0]
  open_images(path_to_dir, name_dir)

def main():
  root_path = '/home/leandrogelain/Documentos/TG/images/mux_l4_4a/soja/slices'
  os.chdir(root_path)
  dirs = os.listdir()
  csv = open('../data_images.csv', 'a+')
  hr = []
  hg = []
  hb = []
  hnir = []
  for i in range(1, 401):
    hr.append(f'r{i}')
    hg.append(f'g{i}')
    hb.append(f'b{i}')
    hnir.append(f'nir{i}')

  head = [hr, hg,hb, hnir]
  head = np.reshape(head, (1,1600))

  header = str(['image_name',list(head[0]), 'crop'])
  
  header = header.replace('[', '').replace(']','')+';\n'
  csv.write(header)

  for dir_name in dirs:
    base_path = (f"{root_path}/{dir_name}")
    write_row_csv(base_path)


main()