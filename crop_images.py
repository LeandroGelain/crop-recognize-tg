import cv2
import os
import sys
import time
import math

def crop_image(image, image_name, base_url):
  os.system(f'cd {base_url}/{image_name}')
  x_total = image.shape[1]
  y_total = image.shape[0]

  size_slice_x = 20
  size_slice_y = 20

  count_row = 1
  count_col = 1

  for row in range(1, y_total, size_slice_y):
    for col in range(1, x_total ,size_slice_x):
      cropped_image = image[((size_slice_x * count_col) - size_slice_x ) : (size_slice_x * count_col), ((size_slice_y * count_row) - size_slice_y ) : (size_slice_y * count_row)]
      if cropped_image.size > 0:
        if cropped_image.shape[0] == size_slice_y and cropped_image.shape[1] == size_slice_x:
          prepare_directories('slices', f"{image_name.split('_')[0]}_{count_col}_{count_row}")
          print(f'created at: {image_name.split("_")[0]}_{count_col}_{count_row}/{image_name.split("_")[0][:-1]}_{image_name.split("_")[1]}.png')
          cv2.imwrite(f'../slices/{image_name.split("_")[0]}_{count_col}_{count_row}/{image_name.split("_")[0][:-1]}_{image_name.split("_")[1]}.png', cropped_image)
      count_col+=1
    count_col = 1
    count_row+=1
  count_row = 1

def prepare_directories(url, image_name):
  if not os.path.exists(f'../{url}/{image_name}'):
    os.makedirs(f'../{url}/{image_name}')

def get_images_path(name_dir):
  os.chdir(f'./images/mux_l4_4a/{name_dir}')
  images = os.listdir()
  paths = []
  names = []
  for image_name in images:
    if image_name[-4:] == '.png':
      names.append(image_name)
      paths.append(f'/{image_name}')
  return paths, names

def get_image(image_path):
  return cv2.imread(image_path)
  
def main():
  base_url = 'soja/nir/'
  images, image_names = get_images_path(base_url)
  
  for image_name in image_names:
    image = get_image(f'./{image_name}')
    crop_image(image, image_name[:-4], os.getcwd())

main()